//
// Created by goktug on 10.04.2021.
//

#ifndef WAYMO_ROSBAG_HANDLER_HELPER_H
#define WAYMO_ROSBAG_HANDLER_HELPER_H

#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <sensor_msgs/Image.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>


#include "waymo_rosbag_handler/Box.h"
#include "waymo_rosbag_handler/CameraImage.h"
#include "waymo_rosbag_handler/CameraImageArray.h"
#include "waymo_rosbag_handler/Label.h"
#include "waymo_rosbag_handler/Lidar.h"
#include "waymo_rosbag_handler/LidarArray.h"
#include "waymo_rosbag_handler/Metadata.h"
#include "waymo_rosbag_handler/VehicleInfo.h"


#include <geometry_msgs/Transform.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_msgs/TFMessage.h>



class Helper
{
public:
    using Point = pcl::PointXYZI;
    using Cloud = pcl::PointCloud<Point>;

    static std::vector<sensor_msgs::PointCloud2>
    ReadLidarTopic(const std::string& path_bag_file, const std::string& topic);

    static std::vector<visualization_msgs::MarkerArray>
    ReadLidarLabel(const std::string& path_bag_file, const std::string& topic);

    static std::vector<sensor_msgs::ImagePtr>
    ReadImageTopic(const std::string& path_bag_file, const std::string& topic);

    static std::vector<visualization_msgs::MarkerArray>
    ReadImageLabel(const std::string& path_bag_file, const std::string& topic);

    static std::vector<tf2_msgs::TFMessage>
    ReadTF(const std::string& path_bag_file, const std::string& topic);

    static void
    PublishPointCloud(sensor_msgs::PointCloud2& msg_cloud,
                              const ros::Publisher& pub_cloud_);




};


#endif //WAYMO_ROSBAG_HANDLER_HELPER_H
