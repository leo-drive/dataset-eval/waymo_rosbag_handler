How to convert Waymo Open Dataset .tfrecord to ROS2 bag .db3

1)
.tfrecord to .bag, use the repo https://github.com/biomotion/waymo-rosbag-record
After docker installed:
sudo systemctl status docker
sudo usermod -aG docker ${USER}
su - ${USER}
./docker_run.sh /home/goktug/Desktop/tf_record /home/goktug/Desktop/undandled_rosbag
catkin_make
source devel/setup.bash
roslaunch waymo_viewer recorder.launch

2)
Output .bag file is failure, use the Waymo Rosbag Handler in order to fix the bag files.

3)
Then, handled rosbag to .proto 
https://github.com/leo-drive/ros1_bridge

4)
Then, proto to ros2bag, use ROS2 Rolling 
https://github.com/leo-drive/proto_to_rosbag2
