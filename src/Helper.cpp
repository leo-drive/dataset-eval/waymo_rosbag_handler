//
// Created by goktug on 10.04.2021.
//

#include "Helper.h"

std::vector<sensor_msgs::PointCloud2>
Helper::ReadLidarTopic(const std::string& path_bag_file,
                           const std::string& topic)
{
    std::vector<sensor_msgs::PointCloud2> all_clouds;

    rosbag::Bag bag;
    bag.open(path_bag_file, rosbag::bagmode::Read);
    rosbag::View view(bag, rosbag::TopicQuery(topic));

    double start;
    double current;

    for(size_t message_id=0; message_id<view.size(); message_id++)
    {
    //std::cout << "Message id: " << message_id << std::endl;
    sensor_msgs::PointCloud::Ptr msg_cloud_PC = std::next(view.begin(), message_id)
            ->instantiate<sensor_msgs::PointCloud>();

    sensor_msgs::PointCloud2 msg_cloud_PC2;
    sensor_msgs::convertPointCloudToPointCloud2(*msg_cloud_PC, msg_cloud_PC2);

    if(message_id == 0)
    {
    start = msg_cloud_PC2.header.stamp.toSec();
    }else{
    current = msg_cloud_PC2.header.stamp.toSec() - start;
    /*std::cout << "Time diff from first message to present: " <<
    current << " second." << std::endl;*/
    }
    if(msg_cloud_PC2.header.frame_id == "/vehicle")
    msg_cloud_PC2.header.frame_id = "vehicle";

    //std::cout << "Topic: " << topic << std::endl;
    //std::cout << "Frame id: " << msg_cloud_PC2.header.frame_id << std::endl;
    if(message_id==0)
        std::cout << "Time stamp: " << msg_cloud_PC2.header.stamp << std::endl;
    all_clouds.push_back(msg_cloud_PC2);
    //std::cout << "********************************************************" << std::endl;
    }
    return all_clouds;
}


void
Helper::PublishPointCloud(sensor_msgs::PointCloud2& msg_cloud,
                          const ros::Publisher& pub_cloud_)

{
    //msg_cloud.header.stamp = ros::Time::now();
    //std::cout << "Original time stamp: " << msg_cloud.header.stamp << std::endl;
    pub_cloud_.publish(msg_cloud);
}



std::vector<sensor_msgs::ImagePtr>
Helper::ReadImageTopic(const std::string &path_bag_file, const std::string &topic)
{
    std::vector<sensor_msgs::ImagePtr> all_images;

    rosbag::Bag bag;
    bag.open(path_bag_file, rosbag::bagmode::Read);
    rosbag::View view(bag, rosbag::TopicQuery(topic));

    for(size_t message_id=0; message_id<view.size(); message_id++)
    {
        //std::cout << "Message id: " << message_id << std::endl;
        sensor_msgs::ImagePtr msg_image_ptr = std::next(view.begin(), message_id)
                ->instantiate<sensor_msgs::Image>();

        if(msg_image_ptr->header.frame_id == "/vehicle")
            msg_image_ptr->header.frame_id = "vehicle";

        //std::cout << "Topic: " << topic << std::endl;
        //std::cout << "Frame id: " << msg_image.header.frame_id << std::endl;
        if(message_id==0)
            std::cout << "Time stamp: " << msg_image_ptr->header.stamp << std::endl;
        all_images.push_back(msg_image_ptr);
        //std::cout << "********************************************************" << std::endl;
    }
    return all_images;
}

std::vector<visualization_msgs::MarkerArray>
Helper::ReadLidarLabel(const std::string &path_bag_file, const std::string &topic)
{
    std::vector<visualization_msgs::MarkerArray> all_lidar_labels;
    rosbag::Bag bag;
    bag.open(path_bag_file, rosbag::bagmode::Read);
    rosbag::View view(bag, rosbag::TopicQuery(topic));
    for(size_t message_id=0; message_id<view.size(); message_id++)
    {
        //std::cout << "Message id: " << message_id << std::endl;
        visualization_msgs::MarkerArrayPtr marker_array_ptr = std::next(view.begin(), message_id)
                ->instantiate<visualization_msgs::MarkerArray>();
        visualization_msgs::MarkerArray markerArray_lid_label = *marker_array_ptr;
        int c = 0;
        for(auto& marker : markerArray_lid_label.markers)
        {
            c++;
            if(marker.header.frame_id == "/vehicle")
                marker.header.frame_id = "vehicle";
            //std::cout << "Frame id: " << marker.header.frame_id << std::endl;
            if(message_id==0 && c==1)
                std::cout << "Time stamp: " << marker.header.stamp << std::endl;
        }
        all_lidar_labels.push_back(markerArray_lid_label);
        //std::cout << "****************************************************" << std::endl;
    }
    return all_lidar_labels;
}

std::vector<visualization_msgs::MarkerArray>
Helper::ReadImageLabel(const std::string &path_bag_file, const std::string &topic)
{
    std::vector<visualization_msgs::MarkerArray> all_image_labels;
    rosbag::Bag bag;
    bag.open(path_bag_file, rosbag::bagmode::Read);
    rosbag::View view(bag, rosbag::TopicQuery(topic));
    for(size_t message_id=0; message_id<view.size(); message_id++)
    {
        visualization_msgs::MarkerArrayPtr marker_array_ptr = std::next(view.begin(), message_id)
                ->instantiate<visualization_msgs::MarkerArray>();
        visualization_msgs::MarkerArray markerArray_cam_label = *marker_array_ptr;
        int c = 0;
        for(auto& marker : markerArray_cam_label.markers)
        {
            c++;
            if(marker.header.frame_id == "/vehicle")
                marker.header.frame_id = "vehicle";
            //std::cout << "Frame id: " << marker.header.frame_id << std::endl;
            if(message_id==0 && c==1)
                std::cout << "Time stamp: " << marker.header.stamp << std::endl;
        }
        all_image_labels.push_back(markerArray_cam_label);
    }
    return all_image_labels;
}

std::vector<tf2_msgs::TFMessage>
Helper::ReadTF(const std::string &path_bag_file, const std::string &topic)
{
    std::vector<tf2_msgs::TFMessage> all_tf;
    rosbag::Bag bag;
    bag.open(path_bag_file, rosbag::bagmode::Read);
    rosbag::View view(bag, rosbag::TopicQuery(topic));
    for(size_t message_id=0; message_id<view.size(); message_id++)
    {
        tf2_msgs::TFMessagePtr transform_ptr_old = std::next(view.begin(), message_id)
                ->instantiate<tf2_msgs::TFMessage>();
        tf2_msgs::TFMessage transform_new;
        auto end_ptr = transform_ptr_old->transforms.end();
        auto start_ptr = transform_ptr_old->transforms.end()-11;
        for(auto it = start_ptr; it!= end_ptr; it++)
        {
            geometry_msgs::TransformStamped new_transform;
            std::string parent_frame = it->header.frame_id;
            std::string child_frame = it->child_frame_id;
            if(parent_frame == "/global" && child_frame == "/vehicle")
            {
                new_transform.header.frame_id = "global";
                new_transform.header.stamp = it->header.stamp;
                new_transform.child_frame_id = "vehicle";
                new_transform.transform.translation.x = it->transform.translation.x;
                new_transform.transform.translation.y = it->transform.translation.y;
                new_transform.transform.translation.z = it->transform.translation.z;
                new_transform.transform.rotation.x = it->transform.rotation.x;
                new_transform.transform.rotation.y = it->transform.rotation.y;
                new_transform.transform.rotation.z = it->transform.rotation.z;
                new_transform.transform.rotation.w = it->transform.rotation.w;
                transform_new.transforms.push_back(new_transform);
            }
        }
        all_tf.push_back(transform_new);
        //std::cout << transform_ptr->transforms.size() << std::endl;
        //std::cout <<  << std::endl;
        //std::cout << "****" << std::endl;
    }
    return all_tf;
}
