//
// Created by goktug on 28.03.2021.
//
#include <ros/ros.h>
#include "Helper.h"

size_t count_msg;
std::vector<std::string> camera_topics =
                                        {"/camera_image/front",
                                         "/camera_image/frontleft",
                                         "/camera_image/frontright",
                                         "/camera_image/sideleft",
                                         "/camera_image/sideright"};
std::vector<std::string> camera_label_topics =
                                        {"/camera_label/front",
                                         "/camera_label/frontleft",
                                         "/camera_label/frontright",
                                         "/camera_label/sideleft",
                                         "/camera_label/sideright"};
std::vector<std::string> lidar_topics =
                                        {"/lidar_pointcloud/front"
                                         "/lidar_pointcloud/left"
                                         "/lidar_pointcloud/rear"
                                         "/lidar_pointcloud/right"
                                         "/lidar_pointcloud/top"};

std::string lidar_label_topic =         "/lidar_label";
std::string tf_topic =                  "/tf";

std::string path_bag_file;
size_t msg_id = 0;
ros::Timer timer_;

std::vector<sensor_msgs::PointCloud2> messages_lidar_front;
ros::Publisher pub_lid_front_;
std::vector<sensor_msgs::PointCloud2> messages_lidar_left;
ros::Publisher pub_lid_left_;
std::vector<sensor_msgs::PointCloud2> messages_lidar_rear;
ros::Publisher pub_lid_rear_;
std::vector<sensor_msgs::PointCloud2> messages_lidar_right;
ros::Publisher pub_lid_right_;
std::vector<sensor_msgs::PointCloud2> messages_lidar_top;
ros::Publisher pub_lid_top_;

std::vector<sensor_msgs::ImagePtr> messages_camera_front;
image_transport::Publisher pub_cam_front_;
std::vector<sensor_msgs::ImagePtr> messages_camera_frontleft;
image_transport::Publisher pub_cam_frontleft_;
std::vector<sensor_msgs::ImagePtr> messages_camera_frontright;
image_transport::Publisher pub_cam_frontright_;
std::vector<sensor_msgs::ImagePtr> messages_camera_sideleft;
image_transport::Publisher pub_cam_sideleft_;
std::vector<sensor_msgs::ImagePtr> messages_camera_sideright;
image_transport::Publisher pub_cam_sideright_;

std::vector<visualization_msgs::MarkerArray> messages_lidar_labels;
ros::Publisher pub_lidar_labels_;

std::vector<visualization_msgs::MarkerArray> messages_cam_labels_front;
ros::Publisher pub_cam_labels_front_;
std::vector<visualization_msgs::MarkerArray> messages_cam_labels_frontleft;
ros::Publisher pub_cam_labels_frontleft_;
std::vector<visualization_msgs::MarkerArray> messages_cam_labels_frontright;
ros::Publisher pub_cam_labels_frontright_;
std::vector<visualization_msgs::MarkerArray> messages_cam_labels_sideleft;
ros::Publisher pub_cam_labels_sideleft_;
std::vector<visualization_msgs::MarkerArray> messages_cam_labels_sideright;
ros::Publisher pub_cam_labels_sideright_;

std::vector<tf2_msgs::TFMessage> messages_tf;

void Callback(const ros::TimerEvent&)
{
   if(msg_id<195)
   {
       Helper::PublishPointCloud(messages_lidar_front[msg_id],pub_lid_front_);
       Helper::PublishPointCloud(messages_lidar_left[msg_id],pub_lid_left_);
       Helper::PublishPointCloud(messages_lidar_rear[msg_id],pub_lid_rear_);
       Helper::PublishPointCloud(messages_lidar_right[msg_id],pub_lid_right_);
       Helper::PublishPointCloud(messages_lidar_top[msg_id],pub_lid_top_);

       pub_cam_front_.publish(messages_camera_front[msg_id]);
       pub_cam_frontleft_.publish(messages_camera_frontleft[msg_id]);
       pub_cam_frontright_.publish(messages_camera_frontright[msg_id]);
       pub_cam_sideleft_.publish(messages_camera_sideleft[msg_id]);
       pub_cam_sideright_.publish(messages_camera_sideright[msg_id]);
       pub_cam_sideright_.publish(messages_camera_sideright[msg_id]);

       pub_lidar_labels_.publish(messages_lidar_labels[msg_id]);

       pub_cam_labels_front_.publish(messages_cam_labels_front[msg_id]);
       pub_cam_labels_frontleft_.publish(messages_cam_labels_frontleft[msg_id]);
       pub_cam_labels_frontright_.publish(messages_cam_labels_frontright[msg_id]);
       pub_cam_labels_sideleft_.publish(messages_cam_labels_sideleft[msg_id]);
       pub_cam_labels_sideright_.publish(messages_cam_labels_sideright[msg_id]);

   }else{
       msg_id = 0;
   }

    msg_id++;
    ROS_INFO("Timer is spun.");
}



int main(int argc, char** argv)
{
    ros::init (argc, argv, "waymo_rosbag_handler_node");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);

    // Read and handle the dataset:
    nh.getParam("path_bagfile",path_bag_file);
    std::cout << "Path file: " << path_bag_file << std::endl;
    rosbag::Bag bag2;
    bag2.open(path_bag_file, rosbag::bagmode::Read);
    rosbag::View view(bag2, rosbag::TopicQuery("/lidar_pointcloud/front"));
    std::cout << "Total message count: " <<  view.size() << std::endl;
    count_msg = view.size();

    // Read lidar topics:
    messages_lidar_front = Helper::ReadLidarTopic(path_bag_file,"/lidar_pointcloud/front");
    pub_lid_front_ = nh.advertise<sensor_msgs::PointCloud2>("/lidar_pointcloud/front",1);
    ROS_INFO("Lidar front has been read.");
    messages_lidar_left = Helper::ReadLidarTopic(path_bag_file,"/lidar_pointcloud/left");
    pub_lid_left_ = nh.advertise<sensor_msgs::PointCloud2>("/lidar_pointcloud/left",1);
    ROS_INFO("Lidar left has been read.");
    messages_lidar_rear = Helper::ReadLidarTopic(path_bag_file,"/lidar_pointcloud/rear");
    pub_lid_rear_ = nh.advertise<sensor_msgs::PointCloud2>("/lidar_pointcloud/rear",1);
    ROS_INFO("Lidar rear has been read.");
    messages_lidar_right = Helper::ReadLidarTopic(path_bag_file,"/lidar_pointcloud/right");
    pub_lid_right_ = nh.advertise<sensor_msgs::PointCloud2>("/lidar_pointcloud/right",1);
    ROS_INFO("Lidar right has been read.");
    messages_lidar_top = Helper::ReadLidarTopic(path_bag_file,"/lidar_pointcloud/top");
    pub_lid_top_ = nh.advertise<sensor_msgs::PointCloud2>("/lidar_pointcloud/top",1);
    ROS_INFO("Lidar top has been read.");
    //Read camera topics:
    messages_camera_front = Helper::ReadImageTopic(path_bag_file,"/camera_image/front");
    pub_cam_front_ = it.advertise("/camera_image/front",1);
    ROS_INFO("Camera front has been read.");
    messages_camera_frontleft = Helper::ReadImageTopic(path_bag_file,"/camera_image/frontleft");
    pub_cam_frontleft_ = it.advertise("/camera_image/frontleft",1);
    ROS_INFO("Camera frontleft has been read.");
    messages_camera_frontright = Helper::ReadImageTopic(path_bag_file,"/camera_image/frontright");
    pub_cam_frontright_ = it.advertise("/camera_image/frontright",1);
    ROS_INFO("Camera frontright has been read.");
    messages_camera_sideleft = Helper::ReadImageTopic(path_bag_file,"/camera_image/sideleft");
    pub_cam_sideleft_ = it.advertise("/camera_image/sideleft",1);
    ROS_INFO("Camera sideleft has been read.");
    messages_camera_sideright = Helper::ReadImageTopic(path_bag_file,"/camera_image/sideright");
    pub_cam_sideright_ = it.advertise("/camera_image/sideright",1);
    ROS_INFO("Camera sideright has been read.");
    // Read lidar labels:
    messages_lidar_labels = Helper::ReadLidarLabel(path_bag_file,"/lidar_label");
    pub_lidar_labels_ = nh.advertise<visualization_msgs::MarkerArray>( "/lidar_label", 0);
    ROS_INFO("Lidar labels have been read.");
    // Read camera labels:
    messages_cam_labels_front = Helper::ReadImageLabel(path_bag_file,"/camera_label/front");
    pub_cam_labels_front_ = nh.advertise<visualization_msgs::MarkerArray>( "/camera_label/front", 0);
    ROS_INFO("Camera labels front have been read.");
    messages_cam_labels_frontleft = Helper::ReadImageLabel(path_bag_file,"/camera_label/frontleft");
    pub_cam_labels_frontleft_ = nh.advertise<visualization_msgs::MarkerArray>( "/camera_label/frontleft", 0);
    ROS_INFO("Camera labels frontleft have been read.");
    messages_cam_labels_frontright = Helper::ReadImageLabel(path_bag_file,"/camera_label/frontright");
    pub_cam_labels_frontright_ = nh.advertise<visualization_msgs::MarkerArray>( "/camera_label/frontright", 0);
    ROS_INFO("Camera labels frontright have been read.");
    messages_cam_labels_sideleft = Helper::ReadImageLabel(path_bag_file,"/camera_label/sideleft");
    pub_cam_labels_sideleft_ = nh.advertise<visualization_msgs::MarkerArray>( "/camera_label/sideleft", 0);
    ROS_INFO("Camera labels sideleft have been read.");
    messages_cam_labels_sideright = Helper::ReadImageLabel(path_bag_file,"/camera_label/sideright");
    pub_cam_labels_sideright_ = nh.advertise<visualization_msgs::MarkerArray>( "/camera_label/sideright", 0);
    ROS_INFO("Camera labels sideright have been read.");
    // Read TF data:
    messages_tf = Helper::ReadTF(path_bag_file,"/tf");
    ROS_INFO("TF messages have been read.");

    rosbag::Bag bag;
    std::string path_output;
    nh.getParam("path_bagfile_output",path_output);
    bag.open(path_output,1);
    for(size_t i=0; i<count_msg; i++)
    {
        std::cout << "Current message id to write into rosbag: " << i  << std::endl;
        bag.write("/lidar_pointcloud/front",messages_lidar_front[i].header.stamp,messages_lidar_front[i]);
        bag.write("/lidar_pointcloud/left",messages_lidar_left[i].header.stamp,messages_lidar_left[i]);
        bag.write("/lidar_pointcloud/rear",messages_lidar_rear[i].header.stamp,messages_lidar_rear[i]);
        bag.write("/lidar_pointcloud/right",messages_lidar_right[i].header.stamp,messages_lidar_right[i]);
        bag.write("/lidar_pointcloud/top",messages_lidar_top[i].header.stamp,messages_lidar_top[i]);

        bag.write("/lidar_label",messages_lidar_labels[i].markers[0].header.stamp,messages_lidar_labels[i]);

        bag.write("/camera_image/front",messages_camera_front[i]->header.stamp,messages_camera_front[i]);
/*        bag.write("/camera_image/frontleft",messages_camera_frontleft[i]->header.stamp,messages_camera_frontleft[i]);
        bag.write("/camera_image/frontright",messages_camera_frontright[i]->header.stamp,messages_camera_frontright[i]);
        bag.write("/camera_image/sideleft",messages_camera_sideleft[i]->header.stamp,messages_camera_sideleft[i]);
        bag.write("/camera_image/sideright",messages_camera_sideright[i]->header.stamp,messages_camera_sideright[i]);*/

        bag.write("/tf",messages_tf[i].transforms[0].header.stamp,messages_tf[i]);

        if(messages_cam_labels_front[i].markers.empty())
        {
            visualization_msgs::Marker marker;
            marker.header.frame_id = "vehicle";
            marker.header.stamp = messages_lidar_front[i].header.stamp;
            messages_cam_labels_front[i].markers.push_back(marker);
        }
        bag.write("/camera_label/front",messages_cam_labels_front[i].markers[0].header.stamp,messages_cam_labels_front[i]);
       /* if(messages_cam_labels_frontleft[i].markers.empty())
        {
            visualization_msgs::Marker marker;
            marker.header.frame_id = "vehicle";
            marker.header.stamp = messages_lidar_front[i].header.stamp;
            messages_cam_labels_frontleft[i].markers.push_back(marker);
        }
        bag.write("/camera_label/frontleft",messages_cam_labels_frontleft[i].markers[0].header.stamp,messages_cam_labels_frontleft[i]);
        if(messages_cam_labels_frontright[i].markers.empty())
        {
            visualization_msgs::Marker marker;
            marker.header.frame_id = "vehicle";
            marker.header.stamp = messages_lidar_front[i].header.stamp;
            messages_cam_labels_frontright[i].markers.push_back(marker);

        }
        bag.write("/camera_label/frontright",messages_cam_labels_frontright[i].markers[0].header.stamp,messages_cam_labels_frontright[i]);
        if(messages_cam_labels_sideleft[i].markers.empty())
        {
            visualization_msgs::Marker marker;
            marker.header.frame_id = "vehicle";
            marker.header.stamp = messages_lidar_front[i].header.stamp;
            messages_cam_labels_sideleft[i].markers.push_back(marker);
        }
        bag.write("/camera_label/sideleft",messages_cam_labels_sideleft[i].markers[0].header.stamp,messages_cam_labels_sideleft[i]);
        if(messages_cam_labels_sideright[i].markers.empty())
        {
            visualization_msgs::Marker marker;
            marker.header.frame_id = "vehicle";
            marker.header.stamp = messages_lidar_front[i].header.stamp;
            messages_cam_labels_sideright[i].markers.push_back(marker);
        }
        bag.write("/camera_label/sideright",messages_cam_labels_sideright[i].markers[0].header.stamp,messages_cam_labels_sideright[i]);*/
    }
    bag.close();
    ROS_INFO("Bag writing is done.");


    //timer_ = nh.createTimer(ros::Duration(0.1), Callback); // 10Hz Waymo dataset

    ros::spin ();
    ros::waitForShutdown();
    return 0;
}

